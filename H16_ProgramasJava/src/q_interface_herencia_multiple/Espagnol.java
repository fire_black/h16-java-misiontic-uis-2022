package q_interface_herencia_multiple;

public class Espagnol extends LenguajeComunicacion implements Idioma{
    // Atributos
    // Constructor

    // Métodos - Funciones

    @Override
    public void decirHola() {
        System.out.println("Quiubo parcero...");
    }

    @Override
    public void buenosDias() {
        System.out.println("Buenos días...");
    }
}
