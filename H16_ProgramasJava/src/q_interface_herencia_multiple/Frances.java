package q_interface_herencia_multiple;

public class Frances extends LenguajeComunicacion implements Idioma  {
    // Atributos
    // Constructor

    // Métodos - Funciones

    @Override
    public void decirHola() {
        System.out.println("Salut...");
    }

    @Override
    public void buenosDias() {
        System.out.println("Bonjour...");
    }
}
