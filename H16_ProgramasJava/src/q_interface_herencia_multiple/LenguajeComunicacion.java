package q_interface_herencia_multiple;

public abstract class LenguajeComunicacion {
    // Atributo
    private boolean latin;

    // Constructor
    public LenguajeComunicacion() {
        this.latin = true;
    }

    public LenguajeComunicacion(boolean latin) {
        this.latin = latin;
    }

    // Métodos - Funciones
    public abstract void buenosDias();

    public void despedida(){
        System.out.println("Expresión de despedida... Siempre mismo mensaje... Clase Padre...");
    }
}
