package s_collections;

import java.util.*;

public class S6_Main_Map_HashMap_TreeMap {

    public static void main(String[] args) {
        System.out.println("------------------------------------------");
        // Ejemplo HashMap
        //Map varHashMap = new HashMap();
        HashMap varHashMap = new HashMap();
        varHashMap.put("clave1", "valor1");
        varHashMap.put("clave2", "valor2");
        varHashMap.put("clave4", "valor4");
        varHashMap.put("clave3", "valor3");
        varHashMap.put("clave3", "valor10");
        varHashMap.put("clave6", "valor6");
        varHashMap.put(6, "trip");
        varHashMap.put(10, 10);
        System.out.println("Mapas tipo HashMap");
        mostrarElementos(varHashMap.keySet());
        mostrarElementos(varHashMap.values());
        System.out.println("------------------------------------------");

        Iterator i = varHashMap.keySet().iterator();
        while (i.hasNext()){
            Object k = i.next();
            if (varHashMap.get(k).equals("valor10")){
                System.out.println("La Clave del Valor6 buscado es: " + k);
            }
            System.out.println("Clave: " + k + "    -   Valor: " + varHashMap.get(k));
        }

        if (varHashMap.containsKey("clave6")){
            System.out.println("La Clave es: " + varHashMap.get("clave6"));
        }
        System.out.println("------------------------------------------");

        System.out.println("Ejemplo de TreeMap");
        // Ejemplo TreeMap
        //Map varTreeMap = new TreeMap();
        //SortedMap varTreeMap = new TreeMap();
        TreeMap varTreeMap = new TreeMap();

        varTreeMap.put(4, "valor 4");
        varTreeMap.put(6, 6);
        varTreeMap.put(2, 2.6);
        varTreeMap.put(1, "uno");
        varTreeMap.put(3, true);
        //varTreeMap.put("10", 10);  // En los TreeMap => Todas las claves deben ser homogéneas.

        mostrarElementos(varTreeMap.keySet());
        mostrarElementos(varTreeMap.values());


        System.out.println("------------------------------------------");


        int a = 3;
        a = 6;

        Object b = 6;
        System.out.println("b = " + b);
        Object c = "Tripulantes";
        System.out.println("c = " + c);
        S_Tripulantes d = new S_Tripulantes();
        System.out.println("d = " + d);
        Object e = d;
        System.out.println("e = " + e);
    }

    public static void mostrarElementos(Collection coll){
        Iterator iter = coll.iterator();
        while (iter.hasNext()){
            //System.out.println("iter.next() = " + iter.next());
            System.out.print(iter.next() + " - ");
        }
        System.out.println();
    }

}
