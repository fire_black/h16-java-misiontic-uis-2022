package s_collections;

import java.util.Iterator;
import java.util.TreeSet;

public class S4_Main_List_TreeSet {
    public static void main(String[] args) {

        System.out.println("------------------------------------------");
        TreeSet<String> tripulantes = new TreeSet<String>();
        tripulantes.add("123");
        tripulantes.add("234");
        tripulantes.add("345");
        String valor = "456";
        tripulantes.add(valor);
        tripulantes.add("456");

        System.out.println("tripulantes = " + tripulantes);
        System.out.println("------------------------------------------");
        System.out.println("Recorrer TreeSet con Foreach");
        for (String i : tripulantes) {
            System.out.println("i = " + i);
        }
        System.out.println("------------------------------------------");
        System.out.println("Recorrer TreeSet con Iterator");
        Iterator<String> i = tripulantes.iterator();
        while (i.hasNext()){
            System.out.println("i desde iterator: " + i.next());
        }
        System.out.println("------------------------------------------");

        /*
        // Creación de Objetos
        String nombre = "Hugo";
        int codigo = 123;
        // Declarar el objeto
        S_Tripulantes trip1;
        // Instanciar el objeto
        trip1 = new S_Tripulantes();
        trip1.mostrarInformacion();
        trip1.setNombre(nombre);
        trip1.setCodigo(codigo);
        trip1.mostrarInformacion();
        System.out.println("Nombre método get: " + trip1.getNombre());
        System.out.println("Código método get: " + trip1.getCodigo());

        System.out.println("------------------------------------------");
        String nombre2 = "Paco";
        int codigo2 = 321;
        S_Tripulantes trip2 = new S_Tripulantes(nombre2, codigo2);
        trip2.mostrarInformacion();

        System.out.println("------------------------------------------");
        S_Tripulantes trip3 = new S_Tripulantes("Luis", 312);
        trip3.mostrarInformacion();

        System.out.println("------------------------------------------");

        // Asignar los Objetos a un arreglo de Objetos - ArryList
        // Declarar e Instanciar el TreeSet List
        TreeSet<S_Tripulantes> tripulantes = new TreeSet<S_Tripulantes>();

        tripulantes.add(trip1);
        tripulantes.add(trip2);
        tripulantes.add(trip3);
        tripulantes.add(new S_Tripulantes("JAS", 666));

        System.out.println("------------------------------------------");
        System.out.println("Uso de foreach => Únicamente recorriendo los elementos de la lista");
        for (S_Tripulantes i : tripulantes) {
            System.out.println("valor que toma i: " + i);
        }

        System.out.println("------------------------------------------");
        System.out.println("Uso de foreach => Únicamente recorriendo los elementos de la lista y mostrando la información");
        for (S_Tripulantes i : tripulantes) {
            System.out.println("valor que toma i: " + i);
            i.mostrarInformacion();
        }

        System.out.println("------------------------------------------");
        */


    }
}
