package s_collections;

import java.util.HashSet;
import java.util.Iterator;

public class S5_Main_List_HashSet {

    public static void main(String[] args) {

        // Creación de Objetos
        String nombre = "Hugo";
        int codigo = 123;
        // Declarar el objeto
        S_Tripulantes trip1;
        // Instanciar el objeto
        trip1 = new S_Tripulantes();
        trip1.mostrarInformacion();
        trip1.setNombre(nombre);
        trip1.setCodigo(codigo);
        trip1.mostrarInformacion();
        System.out.println("Nombre método get: " + trip1.getNombre());
        System.out.println("Código método get: " + trip1.getCodigo());

        System.out.println("------------------------------------------");
        String nombre2 = "Paco";
        int codigo2 = 321;
        S_Tripulantes trip2 = new S_Tripulantes(nombre2, codigo2);
        trip2.mostrarInformacion();

        System.out.println("------------------------------------------");
        S_Tripulantes trip3 = new S_Tripulantes("Luis", 312);
        trip3.mostrarInformacion();

        System.out.println("------------------------------------------");

        // Asignar los Objetos a un arreglo de Objetos - ArryList
        // Declarar e Instanciar el TreeSet List
        HashSet<S_Tripulantes> tripulantes = new HashSet<S_Tripulantes>();

        tripulantes.add(trip1);
        tripulantes.add(trip2);
        tripulantes.add(trip3);
        tripulantes.add(new S_Tripulantes("JAS", 666));

        System.out.println("------------------------------------------");
        System.out.println("Uso de foreach => Únicamente recorriendo los elementos de la lista");
        for (S_Tripulantes i : tripulantes) {
            System.out.println("valor que toma i: " + i);
        }

        System.out.println("------------------------------------------");
        System.out.println("Uso de foreach => Únicamente recorriendo los elementos de la lista y mostrando la información");
        for (S_Tripulantes i : tripulantes) {
            System.out.println("valor que toma i: " + i);
            i.mostrarInformacion();
        }

        System.out.println("------------------------------------------");
        S_Tripulantes trip5 = trip1;
        tripulantes.add(trip5);
        System.out.println("Recorrido con Iterator");
        Iterator<S_Tripulantes> iterator = tripulantes.iterator();
        while (iterator.hasNext()){
            System.out.println("valor de iterator: " + iterator.next());
        }
        System.out.println("------------------------------------------");
        S_Tripulantes trip6 = new S_Tripulantes("Hugo3", 123);
        tripulantes.add(trip6);
        System.out.println("Recorrido con Iterator - Mostrando la información");
        Iterator<S_Tripulantes> i = tripulantes.iterator();
        while (i.hasNext()){
            S_Tripulantes copTrip = i.next();
            System.out.println("Nombre: " + copTrip.getNombre());
            System.out.println("Código: " + copTrip.getCodigo());
        }
        System.out.println("------------------------------------------");

    }
}
