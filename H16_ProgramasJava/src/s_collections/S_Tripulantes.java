package s_collections;

import java.util.Objects;

public class S_Tripulantes {
    // Atrubutos
    private String nombre;
    private int codigo;

    // Constructores
    public S_Tripulantes(){}

    public S_Tripulantes(String nombre, int codigo) {
        this.nombre = nombre;
        this.codigo = codigo;
    }

    // Métodos - Funciones
    public void mostrarInformacion(){
        System.out.println("Nombre: " + this.nombre);
        System.out.println("Código: " + this.codigo);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof S_Tripulantes)) return false;
        S_Tripulantes that = (S_Tripulantes) o;
        return getCodigo() == that.getCodigo();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo());
    }
}
