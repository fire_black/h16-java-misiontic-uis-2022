package s_collections;

import java.util.Iterator;
import java.util.LinkedList;

public class S3_Main_List_LinkedList {

    public static void main(String[] args) {

        // Creación de Objetos
        String nombre = "Hugo";
        int codigo = 123;
        // Declarar el objeto
        S_Tripulantes trip1;
        // Instanciar el objeto
        trip1 = new S_Tripulantes();
        trip1.mostrarInformacion();
        trip1.setNombre(nombre);
        trip1.setCodigo(codigo);
        trip1.mostrarInformacion();
        System.out.println("Nombre método get: " + trip1.getNombre());
        System.out.println("Código método get: " + trip1.getCodigo());

        System.out.println("------------------------------------------");
        String nombre2 = "Paco";
        int codigo2 = 321;
        S_Tripulantes trip2 = new S_Tripulantes(nombre2, codigo2);
        trip2.mostrarInformacion();

        System.out.println("------------------------------------------");
        S_Tripulantes trip3 = new S_Tripulantes("Luis", 312);
        trip3.mostrarInformacion();

        System.out.println("------------------------------------------");

        // Asignar los Objetos a un arreglo de Objetos - ArryList
        // Declarar e Instanciar el Linked List
        LinkedList<S_Tripulantes> tripulantes = new LinkedList<S_Tripulantes>();
        tripulantes.add(trip1);
        tripulantes.add(trip2);
        tripulantes.add(trip3);
        tripulantes.add(new S_Tripulantes("JAS", 666));

        System.out.println("Uso de fori => Únicamente recorriendo los elementos de la lista");
        for (int i = 0; i < tripulantes.size(); i++) {
            System.out.println("tripulantes[" + i + "]: " + tripulantes.get(i));
        }
        System.out.println("------------------------------------------");
        System.out.println("Uso de fori => Únicamente recorriendo los elementos de la lista y mostrando la información");
        for (int i = 0; i < tripulantes.size(); i++) {
            System.out.println("tripulantes[" + i + "]: ");
            tripulantes.get(i).mostrarInformacion();
        }
        System.out.println("------------------------------------------");
        System.out.println("Uso de foreach => Únicamente recorriendo los elementos de la lista");
        for (S_Tripulantes i : tripulantes) {
            System.out.println("valor que toma i: " + i);
        }

        System.out.println("------------------------------------------");
        System.out.println("Uso de foreach => Únicamente recorriendo los elementos de la lista y mostrando la información");
        for (S_Tripulantes i : tripulantes) {
            System.out.println("valor que toma i: " + i);
            i.mostrarInformacion();
        }

        System.out.println("------------------------------------------");

        System.out.println("Recorrer los elementos y eliminar o remover el objeto cuyo  nombre es paco");
        Iterator<S_Tripulantes> k_varIteradora = tripulantes.iterator();
        while (k_varIteradora.hasNext()){
            S_Tripulantes copiaObj = k_varIteradora.next();
            String nombreObj = copiaObj.getNombre();
            //if (copiaObj.getNombre().equals("Paco")){
            if (nombreObj.equals("Paco")){
                System.out.println("Objeto Eliminado: " + copiaObj);
                k_varIteradora.next();
                tripulantes.remove(copiaObj); // Remover el elemento teniendo en cuenta el objeto.
                // tripulantes.remove(tripulantes.indexOf(copiaObj));  // Remover el elemento teniento en cuenta el índice del objeto.
            }
            /*else {
                System.out.println("Nombre método get: " + copiaObj.getNombre());
                System.out.println("Código método get: " + copiaObj.getCodigo());
            }*/
            
        }
        

        System.out.println("------------------------------------------");
        System.out.println("Recorrer la Lista con Iterator");
        Iterator<S_Tripulantes> i = tripulantes.iterator();
        while (i.hasNext()){
            /*i.next().mostrarInformación();
            System.out.println("Nombre: " + i.next().getNombre());
            System.out.println("Código: " + i.next().getCodigo());*/
            S_Tripulantes copiaDei = i.next();
            System.out.println("valor de i.next(): " + copiaDei);
            copiaDei.mostrarInformacion();
            System.out.println("Nombre método get: " + copiaDei.getNombre());
            System.out.println("Código método get: " + copiaDei.getCodigo());
        }

        System.out.println("------------------------------------------");

    }
}
