package x_ejemplo_db_jdbc.controlador_persistencia;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConexionBD {
    // Atributos
    private String url = "";   // ruta de la base de datos
    private Connection con = null;   // establece la conexion con la base de datos
    private Statement stmt = null;   // estado de la conexión
    private ResultSet rs = null;   // asignación de resultados

    // Constructor
    public ConexionBD(){
        url = "jdbc:sqlite:db_ejemplo.db";
        try {
            con = DriverManager.getConnection(url);
            if (con != null){
                DatabaseMetaData metaData = con.getMetaData();
                System.out.println("Conexión exitosa... MetaData: ");
                System.out.println(metaData.getDriverName());
            }
        } catch (SQLException e){
            System.out.println("Conexión errónea... e: " + e.getMessage());
        }
    }

    // Métodos - Funciones
    // Retorna el estado de la conexión
    public Connection getConnection(){
        return con;
    }

    // Cierre de la conexión a la base de datos
    public void  closeConnection(Connection con){
        if (con != null){
            try {
                con.close();
            } catch (SQLException e){
                Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, e);
                System.out.println("no se pudo cerrar la conexión...");
            }
        }
    }

    // CRUD
    // Create - Insert => Agregar o insertar valores en la db
    public boolean insertetarBD(String sentencia_sql){
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia_sql);
        } catch (SQLException | RuntimeException e){
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    // Read - Select => Realiza consultas a la db
    public ResultSet consultarBD(String sentencia_sql){
        try {
            stmt = con.createStatement();
            stmt.executeQuery(sentencia_sql);
        } catch (SQLException e){
            System.out.println(e.getMessage());
        } catch (RuntimeException rt){
            System.out.println(rt.getMessage());
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return rs;
    }

    // Update => Actualizar o Modificar valores en la db
    public boolean actualizarBD(String sentencia_sql){
        try {
            stmt = con.createStatement();
            stmt.executeUpdate(sentencia_sql);
        } catch (SQLException | RuntimeException e){
            System.out.println("Error de actualización: " + e.getMessage());
            return false;
        }
        return true;
    }

    // Delete => Borrar o Eliminar valores de la db
    public boolean borrarBD(String sentencia_sql){
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia_sql);
        } catch (SQLException | RuntimeException e){
            System.out.println("Error al borrar: " + e.getMessage());
            return false;
        }
        return true;
    }

    // AutoGuardado
    public boolean setAutoCommitBD(boolean b){
        try {
            con.setAutoCommit(b);
        } catch (SQLException e){
            System.out.println("Error de AutoCommit: " + e.getMessage());
            return false;
        }
        return true;
    }

    // Cerrar la conexión, teniendo en cuenta el atributo de la clase Connection
    public void cerrarConexion(){
        closeConnection(con);
    }

    // Confirmar la sentencia o validar cambios => Commit
    public boolean commitBD(){
        try {
            con.commit();
        } catch (SQLException e){
            System.out.println("Error de commit: " + e.getMessage());
            return false;
        }
        return true;
    }

    // Cancelar sentencia o cambios sql en la db  => rollbackBD
    public boolean rollbackBD(){
        try {
            con.rollback();
        } catch (SQLException e){
            System.out.println("Error de rollback: " + e.getMessage());
            return false;
        }
        return true;
    }
}
