package x_ejemplo_db_jdbc;

import x_ejemplo_db_jdbc.controlador_persistencia.ConexionBD;

import java.sql.Connection;
import java.sql.ResultSet;

public class Main {

    public static void main(String[] args) {

        ConexionBD obj = new ConexionBD();

        try (Connection cnx = obj.getConnection()) {
            System.out.println("Conexión Ok...");
            ResultSet rs = obj.consultarBD("select * from productos;");
            System.out.println(rs);
        } catch (Exception e){
            System.out.println("Se produjo un error....");
            System.out.println(e);
        }
    }
}
