package m_agregacion_vs_composicion;

public class Celular {
    // Atributos
    private String fabricante, modelo;
    private long imei;
    private Bateria bateria;
    private byte cantSim;
    // private SimCard [] simCard;
    private SimCard simCard[];

    // Constructores

    public Celular() {
        // Composición => teniendo en cuenta que celuar no existe sin bateria
        this.bateria = new Bateria();
    }

    public Celular(String fabricante, String modelo, long imei, String refBat, float ampBat, byte maxCantSim) {
        this.fabricante = fabricante;
        this.modelo = modelo;
        this.imei = imei;
        // Composición => creo o instancio el objeto batería desde el constructor de la clase Celular
        this.bateria = new Bateria(refBat, ampBat);
        // Agregación => defino que inicia en cero la cantSim
        this.cantSim = 0;
        // Agregación => defino la cantidad máxima que puedo agregar de objetos de la clase SimCard
        this.simCard = new SimCard[maxCantSim]; // máximo puedo agregar maxCantSim = 4
    }

    // Métodos - Funciones
    public void agregarSimCard(SimCard simCard){
        if (this.cantSim < this.simCard.length){
            this.simCard[this.cantSim] = simCard;
            this.cantSim++;
        }
        else{
            System.out.println("No es posible agregar más simcard al celular...");
        }
    }

    public void mostrarInformacion(){
        System.out.println("Fabrecante: " + this.fabricante);
        System.out.println("Modelo: " + this.modelo);
        System.out.println("Imei: " +  this.imei);
        this.bateria.informacionBateria();
        System.out.println("Cantidad SimCard Agregadas: " + this.cantSim);
        for (int i = 0; i < this.cantSim; i++) {
            this.simCard[i].informacionSimCard();
        }
    }
}
