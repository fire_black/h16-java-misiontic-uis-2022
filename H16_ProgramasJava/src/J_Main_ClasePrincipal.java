public class J_Main_ClasePrincipal {

    public static void main(String[] args) {

        // Funciones Estáticas NO Requieren Cnstanciar/Crear el objeto
        J_ImprimirMensaje_ClaseSecundaria.funMensaje1();

        // Funciones Dinámicas => Requieren Instanciar/Crear el Objeto
        // Declarar el Objeto
        J_ImprimirMensaje_ClaseSecundaria nombreObjeto;
        // Instanciar el Objeto
        nombreObjeto = new J_ImprimirMensaje_ClaseSecundaria("Mensaje 2 - Instanciado el Objeto");

        // Para utilizar la función dinámica => utilizo el objeto creado...
        nombreObjeto.funMensaje2();

        J_ImprimirMensaje_ClaseSecundaria nombObj2 = new J_ImprimirMensaje_ClaseSecundaria();
        System.out.println("Constructor con Objeto Vacío:");
        nombObj2.funMensaje2();

        J_ImprimirMensaje_ClaseSecundaria nombObj3 = new J_ImprimirMensaje_ClaseSecundaria("Mensaje 3 - Blaaaaaa....");
        nombObj3.funMensaje2();
    }
}
