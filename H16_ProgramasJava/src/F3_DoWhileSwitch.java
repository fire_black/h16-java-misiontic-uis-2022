import java.util.Scanner;

public class F3_DoWhileSwitch {

    public static void main(String[] args) {

        // Declarar Variables
        Scanner leerDatos = new Scanner(System.in);
        String opcion;
        int cont;

        // Ejecución del Programa

        do {
            System.out.println("####### Menú de Opciones ######");
            System.out.println("1. Ejecutar while");
            System.out.println("2. Ejecutar do-while");
            System.out.println("3. Ejecutar for");
            System.out.println("4. Operador Ternario");
            System.out.println("0. Terminar o Salir");
            System.out.println("Digite un número del menú anterior: ");
            opcion = leerDatos.next();

            switch (opcion){
                case "1":
                    //int cont = 0;
                    cont = 0;
                    while (cont < 6){
                        System.out.println("cont = " + cont);
                        cont++;
                    }
                    System.out.println("cont finaliza en: " + cont);
                    break;

                case "2":
                    cont = 0;
                    do {
                        System.out.println("cont = " + cont);
                        cont += 3;
                    } while (cont < 10);
                    System.out.println("cont finaliza en: " + cont);
                    break;

                case "3":
                    //for (int i = 0; i < 3; i++) {
                    int i; // la declaro
                    for (i = 0; i < 3; i++) {
                        System.out.println("i = " + i);
                    }
                    System.out.println("i finaliza en: " + i);
                    break;

                case "4":
                    System.out.println("Digite \"S\" o \"N\"");
                    String texto = leerDatos.next();
                    System.out.println("texto = " + texto);

                    texto = (texto.equals("S"))? "Seleccionó S": "Seleccionó N";
                    System.out.println("texto = " + texto);
                    break;

                case "0":
                    System.out.println("Realmente desea finalizar el Programa (S/N)?");
                    opcion = leerDatos.next();

                    if (opcion.equals("S") || opcion.equals("s")){
                        opcion = "0";
                        System.out.println("Gracias por utilizar SoftMinTIC UIS...!");
                        System.out.println("Desarrolladores de Software Profesional...!");
                    }
                    else { // redundancia de código
                        opcion = "";
                    }
                    break;

                default:
                    System.out.println("Por favor seleccione una opción válida del menú");
                    break;

            }
        } while (!opcion.equals("0") && opcion != null);

    }
}
