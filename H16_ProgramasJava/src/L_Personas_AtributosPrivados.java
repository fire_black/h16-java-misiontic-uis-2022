public class L_Personas_AtributosPrivados {
    // Atributos
    private int id;
    private String nombre, apellido;


    // Constructor


    public L_Personas_AtributosPrivados() {
    }
    // Polimorfismo de Sobrecarga => el mismo nombre de la función (L_Personas_AtributosPrivados), que recibe diferentes parámetros o argumentos

    public L_Personas_AtributosPrivados(int id, String nombre, String apellido) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    // Métodos - Funciones

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
}
