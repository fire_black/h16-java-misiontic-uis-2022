import java.util.Scanner;

public class D2_convPesosDolaresFuncion {

    public static void main(String[] args) {
        // Programa para convertir de pesos colombianos a dólares utilizando una función.
        // Declaración de Variables
        Scanner input = new Scanner(System.in);
        float trm, cantPesosTengo, cantDolares;  // trm => tasa representativa monetaria del dólar

        // Entrada(s)
        // trm = 3912.15f;
        // cantPesosTengo = 10000f;
        System.out.println("Digite la cantidad de pesos que tiene:");
        cantPesosTengo = input.nextFloat();

        // Transformación - Llamado a una Función
        //cantDolares = cantPesosTengo * 1 / trm;
        cantDolares = convPesosDolares(cantPesosTengo);

        // Salida(s)
        System.out.println("Ud, tiene USD$" + cantDolares + " dólares.");

        // llamado otra función
        System.out.println("Función Dólares-Pesos: " + convDolaresPesos(1000));

    }

    public static float convPesosDolares(float pesos){
        // Declarar Variables
        float cantDolares, trm;
        // Entrada
        trm = 3912.15f;
        // Transformación
        cantDolares = pesos * 1 / trm ;
        // Salida
        return cantDolares;
    }

    public static float convDolaresPesos(float dolares){
        // Declarar Variables
        float trm, cantPesos;
        // Entrada
        trm = 3912.15f;
        // Transformación
        cantPesos = dolares * trm / 1;
        // Salida
        return cantPesos;
    }

}
