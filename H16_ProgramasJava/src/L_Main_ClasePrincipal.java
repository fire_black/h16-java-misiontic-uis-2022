public class L_Main_ClasePrincipal {

    public static void main(String[] args) {

        //L_Personas_ClaseSecundaria.id = 100;  // permite ejecutarla cuando el modificador de acceso es diferente de privado
        // Se ejecutan con respecto al atributo privado del id
        System.out.println("id = " + L_Personas_ClaseSecundaria.infoId());
        L_Personas_ClaseSecundaria.asignarId(100); // asignando el valor directamente al atributo id, mediante la función asignar id
        System.out.println("id = " + L_Personas_ClaseSecundaria.infoId());

        L_Personas_ClaseSecundaria p1 = new L_Personas_ClaseSecundaria(); // Instanciado con el constructor por defecto
        System.out.println("Función por Defecto toString: " + p1.toString());
        System.out.println("Objeto Creado p1: " + p1);

        /*--------------------------*/
        // Continuar con Funciones Getter and Setter...

        // Cuando son atributos privados debo crear el objeto
        L_Personas_AtributosPrivados p2 = new L_Personas_AtributosPrivados();
        System.out.println("Objeto p2: " + p2);
        System.out.println("p2 id: " + p2.getId());
        System.out.println("p2 nombre: " + p2.getNombre());
        System.out.println("p2 apellido: " + p2.getApellido());

        p2.setId(123);
        p2.setNombre("Hugo");
        p2.setApellido("Montecristo");
        System.out.println("p2 id: " + p2.getId());
        System.out.println("p2 nombre: " + p2.getNombre());
        System.out.println("p2 apellido: " + p2.getApellido());

        L_Personas_AtributosPrivados p3 = new L_Personas_AtributosPrivados(321, "Paco", "Ruiz");
        System.out.println("Objeto p3: " + p3);
        System.out.println("p3 id: " + p3.getId());
        System.out.println("p3 nombre: " + p3.getNombre());
        System.out.println("p3 apellido: " + p3.getApellido());

        p3.setApellido("Morales");
        System.out.println("Objeto p3: " + p3);
        System.out.println("p3 id: " + p3.getId());
        System.out.println("p3 nombre: " + p3.getNombre());
        System.out.println("p3 apellido: " + p3.getApellido());

        /*--------------------------*/
        // Consecutivo Incremental
        L_Personas_IdConsecutivo p4 = new L_Personas_IdConsecutivo();
        System.out.println("Objeto p4: " + p4);
        System.out.println("p4 id: " + p4.getId());
        System.out.println("p4 nombre: " + p4.getNombre());
        System.out.println("p4 apellido: " + p4.getApellido());

        p4.setNombre("Luis");
        p4.setApellido("López");
        System.out.println("Objeto p4: " + p4);
        System.out.println("p4 id: " + p4.getId());
        System.out.println("p4 nombre: " + p4.getNombre());
        System.out.println("p4 apellido: " + p4.getApellido());


        L_Personas_IdConsecutivo p5 = new L_Personas_IdConsecutivo();
        System.out.println("Objeto p5: " + p5);
        System.out.println("p5 id: " + p5.getId());
        System.out.println("p5 nombre: " + p5.getNombre());
        System.out.println("p5 apellido: " + p5.getApellido());

        L_Personas_IdConsecutivo p6 = new L_Personas_IdConsecutivo("JAS", "JAS");
        System.out.println("Objeto p6: " + p6);
        System.out.println("p6 id: " + p6.getId());
        System.out.println("p6 nombre: " + p6.getNombre());
        System.out.println("p6 apellido: " + p6.getApellido());

        System.out.println("---------------------------------");
        System.out.println("p4 id: " + p4.getId());
        System.out.println("p5 id: " + p5.getId());
        System.out.println("p6 id: " + p6.getId());
        System.out.println("---------------------------------");

        System.out.println("Método toString de p6: " + p6.toString());

        p6.setNombre("Jahir");
    }
}
