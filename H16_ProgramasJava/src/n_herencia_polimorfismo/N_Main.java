package n_herencia_polimorfismo;

public class N_Main {

    public static void main(String[] args) {

        Circulo circulo = new Circulo("Círculo", (byte) 1, 11.5);
        System.out.println("Área del " + circulo.getNombre() + ": " + circulo.areaFigura());

        Cuadrado cuadrado = new Cuadrado("Cuadrado", (byte) 4, 10.6);
        System.out.println("Área del " + cuadrado.getNombre() + ": " + cuadrado.areaFigura());

        cuadrado.setNombre("Cuadradito");
        System.out.println("Área del " + cuadrado.getNombre() + ": " + cuadrado.areaFigura());


        FigurasGeometricas figuras [] = new FigurasGeometricas[10];
        figuras[0] = circulo;
        figuras[1] = cuadrado;

        int vector [] = new int[3];
        vector[0] = 6;
        //vector[1] = "Trip...";

    }
}
