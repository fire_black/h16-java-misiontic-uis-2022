package n_herencia_polimorfismo;

public class Circulo extends FigurasGeometricas {

    // Atributos
    private double radio;

    // Constructor
    public Circulo(){}

    public Circulo(String nombre, byte cantLados, double radio){
        super(nombre, cantLados);
        this.radio = radio;
    }

    // Métodos - Funciones
    @Override
    public double areaFigura(){
        double area = Math.PI * Math.pow(this.radio, 2);
        return Math.round(area * 100.0) / 100.0;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
}
