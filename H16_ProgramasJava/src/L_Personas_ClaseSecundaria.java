public class L_Personas_ClaseSecundaria {

    // Atributos
    private static int id;
    String nombre, apellido;

    // Métodos - Funciones
    // Método para obtener el valor privado del atributo id
    public static int infoId(){
        return id;
    }

    // Método para asignar el valor privado del atributo id
    static void asignarId(int id2){
        id = id2;
    }
}
