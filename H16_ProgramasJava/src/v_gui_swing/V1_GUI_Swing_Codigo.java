package v_gui_swing;

import javax.swing.*;

public class V1_GUI_Swing_Codigo extends JFrame {

    // Atributos
    private JLabel label1;

    // Constructor
    public V1_GUI_Swing_Codigo(){
        label1 = new JLabel("Hola Tripulantes");
        label1.setBounds(20, 50, 200, 15);
        add(label1);

        JLabel label2 = new JLabel("SoftMinTIC UIS ver. 0.0.1");
        label2.setBounds(20, 150, 150, 20);
        add(label2);
    }

    // Métodos - Funciones
    public static void main(String[] args) {
        V1_GUI_Swing_Codigo formulario = new V1_GUI_Swing_Codigo();
        // setBounds => Permite definir o establecer la posición y el tamaño del objeto
        formulario.setBounds(5, 10, 600, 600);
        formulario.setVisible(true); // Permite definir la visibilidad de la ventana
        formulario.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // permite establecer la operación una vez haga click en cerrar la ventana
    }
}
