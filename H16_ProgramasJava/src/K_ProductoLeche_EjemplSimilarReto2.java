public class K_ProductoLeche_EjemplSimilarReto2 {

    /*La Empresa X, distribuye producto lácteos, entre ellos
     * Leche deslatosada y Leche entera en empaques de Litro.
     * Si la leche es deslatosada el precio es de 4000
     * Si la leche no es deslatosada el precio es de 3000.
     * Llegado el caso que un minimercado realice la cotización,
     * Se debe generar la siguiente información:
     * Nombre, Código, Categoría, Descripción (Entera/Deslatosada), Empresa,
     * Precio (el cual debe tenerse en cuenta si es deslatosada o no).
     * Tener en cuenta el diagrama de clases Diseñado Previamente.*/

    // Atributos
    private String nombre;
    private String codigo, categoria, descripcion, empresa;
    private boolean deslatosada;

    // Métodos - Funciones

    // Constructor
    K_ProductoLeche_EjemplSimilarReto2(){}

    public K_ProductoLeche_EjemplSimilarReto2(String nombre, String codigo, String categoria, String descripcion, String empresa, boolean deslatosada){
        this.nombre = nombre;
        this.codigo = codigo;
        this.categoria = categoria;
        this.descripcion = descripcion;
        this.empresa = empresa;
        this.deslatosada = deslatosada;
    }

    // Función Precio del Producto
    public float precio(){
        float precioProd;
        // if (this.deslatosada == true){
        if (this.deslatosada){
            precioProd = 4000f;
        }
        else {
            precioProd = 3000f;
        }
        return precioProd;
    }

    // Función Info => Imprime la Información del Producto
    public void info(){
        System.out.println("Información del Producto:");
        System.out.println("Nombre: " + this.nombre);
        System.out.println("Código: " + this.codigo);
        System.out.println("Categoría: " + this.categoria);
        System.out.println("Descripción: " + this.descripcion);
        System.out.println("Empresa: " + this.empresa);
        System.out.println("Precio: " + this.precio());
    }
}
