package p_clases_abstract_interface;

public class P_Main {

    public static void main(String[] args) {

        System.out.println("----------------------------------------------------");
        System.out.println("Tamaño en bits tipo dato byte: " + Byte.SIZE);
        System.out.println("Tamaño en Bytes tipo dato byte: " + Byte.BYTES);
        System.out.println("Mínimo valor # tipo dato byte: " + Byte.MIN_VALUE);
        System.out.println("Máximo valor # tipo dato byte: " + Byte.MAX_VALUE);
        System.out.println("----------------------------------------------------");
        System.out.println("Tamaño en bits tipo dato short: " + Short.SIZE);
        System.out.println("Tamaño en Bytes tipo dato short: " + Short.BYTES);
        System.out.println("Mínimo valor # tipo dato short: " + Short.MIN_VALUE);
        System.out.println("Máximo valor # tipo dato short: " + Short.MAX_VALUE);
        System.out.println("----------------------------------------------------");
        System.out.println("Tamaño en bits tipo dato int: " + Integer.SIZE);
        System.out.println("Tamaño en Bytes tipo dato int: " + Integer.BYTES);
        System.out.println("Mínimo valor # tipo dato int: " + Integer.MIN_VALUE);
        System.out.println("Máximo valor # tipo dato int: " + Integer.MAX_VALUE);
        System.out.println("----------------------------------------------------");
        System.out.println("Tamaño en bits tipo dato long: " + Long.SIZE);
        System.out.println("Tamaño en Bytes tipo dato long: " + Long.BYTES);
        System.out.println("Mínimo valor # tipo dato long: " + Long.MIN_VALUE);
        System.out.println("Máximo valor # tipo dato long: " + Long.MAX_VALUE);
        System.out.println("----------------------------------------------------");
        System.out.println("Tamaño en bits tipo dato float: " + Float.SIZE);
        System.out.println("Tamaño en Bytes tipo dato float: " + Float.BYTES);
        System.out.println("Mínimo valor # tipo dato float: " + Float.MIN_VALUE);
        System.out.println("Máximo valor # tipo dato float: " + Float.MAX_VALUE);
        System.out.println("----------------------------------------------------");
        System.out.println("Tamaño en bits tipo dato double: " + Double.SIZE);
        System.out.println("Tamaño en Bytes tipo dato double: " + Double.BYTES);
        System.out.println("Mínimo valor # tipo dato double: " + Double.MIN_VALUE);
        System.out.println("Máximo valor # tipo dato double: " + Double.MAX_VALUE);
        System.out.println("----------------------------------------------------");
        System.out.println("Tamaño en bits tipo dato char: " + Character.SIZE);
        System.out.println("Tamaño en Bytes tipo dato char: " + Character.BYTES);
        System.out.println("Mínimo valor # tipo dato char: " + Character.MIN_VALUE);
        System.out.println("Máximo valor # tipo dato char: " + Character.MAX_VALUE);
        System.out.println("----------------------------------------------------");
    }
}
