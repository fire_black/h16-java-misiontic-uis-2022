package r_reto4_o_reto3_ejersimilar.reto4_agregar_lista;

import r_reto4_o_reto3_ejersimilar.o_reto3_ejersimilar.Producto;

import java.util.ArrayList;
import java.util.List;

public class VitrinaRefrigerador {

    // Atributos
    private List<Producto> productos;

    // Constructor
    public VitrinaRefrigerador() {
        this.productos = new ArrayList<Producto>();
    }

    // Métodos - Funciones
    public void agregarProducto(Producto producto){
        this.productos.add(producto);
    }

    public void mostrarProductos(){
        for (Producto prod : this.productos) {
            System.out.println("Producto: " + prod);
        }
    }
}
