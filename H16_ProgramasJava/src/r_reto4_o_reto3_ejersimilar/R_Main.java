package r_reto4_o_reto3_ejersimilar;

import r_reto4_o_reto3_ejersimilar.o_reto3_ejersimilar.ProductoNoRefrigerado;
import r_reto4_o_reto3_ejersimilar.o_reto3_ejersimilar.ProductoRefrigerado;
import r_reto4_o_reto3_ejersimilar.reto4_agregar_lista.VitrinaRefrigerador;

public class R_Main {
    public static void main(String[] args) {

        VitrinaRefrigerador victRef = new VitrinaRefrigerador();

        victRef.agregarProducto(new ProductoNoRefrigerado("Pan Largo", "1230", 2000f, 25f));
        victRef.agregarProducto(new ProductoRefrigerado("Leche Entera", "1231", 3000f, 15f));

        victRef.mostrarProductos();

    }
}
