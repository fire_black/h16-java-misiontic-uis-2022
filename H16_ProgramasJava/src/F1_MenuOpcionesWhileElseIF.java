import java.util.Scanner;

public class F1_MenuOpcionesWhileElseIF {
    public static void main(String[] args) {
        // Declarar Variables
        Scanner leerDatos = new Scanner(System.in);
        int opcion = 6, op;

        while (opcion != 0){
            System.out.println("####### Menú de Opciones ######");
            System.out.println("1. Primer caso...");
            System.out.println("2. Segundo caso...");
            System.out.println("3. Tercero caso...");
            System.out.println("4. Cuarto caso...");
            System.out.println("0. Terminar o Salir");
            System.out.println("Digite un número del menú anterior: ");
            opcion = leerDatos.nextInt();

            if(opcion == 1){
                System.out.println("Seleccionó la opción 1...");
                System.out.println("Digite enter para continuar...");
                op = leerDatos.nextInt();
            }
            else if (opcion == 2){
                System.out.println("Seleccionó la opción 2...");
                System.out.println("Digite enter para continuar...");
                op = leerDatos.nextInt();
            }
            else if (opcion == 3){
                System.out.println("Seleccionó la opción 3...");
                System.out.println("Digite enter para continuar...");
                op = leerDatos.nextInt();
            }
            else if (opcion == 4){
                System.out.println("Seleccionó la opción 4...");
                System.out.println("Digite enter para continuar...");
                op = leerDatos.nextInt();
            }
            else if (opcion == 0){
                System.out.println("Seleccionó la opción 0...");
                System.out.println("Digite enter para continuar...");
                op = leerDatos.nextInt();
            }
            else {
                System.out.println("Por favor seleccione una opción válida del menú...");
                System.out.println("Digite enter para continuar...");
                op = leerDatos.nextInt();
            }
        }
    }
}
